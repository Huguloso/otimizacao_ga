// A C++ program to find convex hull of a set of points. Refer
// https://www.geeksforgeeks.org/orientation-3-ordered-points/
// for explanation of orientation()

using System.Collections.Generic;
using System.Linq;
using csDelaunay;
using UnityEngine;

public class GraphManager : MonoBehaviour
{
    // A global point needed for  sorting points with reference
    // to  the first point Used in compare function of qsort()
    public static Vector2f P0;

    [SerializeField] private List<Vector2f> _pointsList;
    [SerializeField] private int _pointsRange = 500;
    [SerializeField] private int _numberOfPoints = 100;
    [SerializeField] private bool _randomPoints = true;

    private List<Edge> _voronoiEdges;
    private List<Vector2f> _pointsInHull;
    private Voronoi _voronoi;

    public List<Vector2f> PointsList => _pointsList;
    public List<Edge> VoronoiEdges => _voronoiEdges;

    public List<Vector2f> PointsInHull => _pointsInHull;

    private void Awake()
    {
        InitializeGraph();
    }

    public void InitializeGraph()
    {
        if (_randomPoints)
        {
            _pointsList = CreateRandomPoint();
        }

        ConvexHull(_pointsList, _pointsList.Count);
        GenerateVoronoi(_pointsList);
        Pathfinding.GeneratePathNodes(_voronoi.SitesIndexedByLocation);
    }

    private List<Vector2f> CreateRandomPoint()
    {
        List<Vector2f> points = new List<Vector2f>();

        for (int i = 0; i < _numberOfPoints; i++)
        {
            points.Add(new Vector2f(Random.Range(-_pointsRange, _pointsRange), Random.Range(-_pointsRange, _pointsRange)));
        }

        return points;
    }

    private void ConvexHull(List<Vector2f> points, int numberOfPoints)
    {
        float yMin = points[0].y;
        int minIndex = 0;

        for (int i = 1; i < numberOfPoints; i++)
        {
            float y = points[i].y;

            if (y < yMin || yMin == y && points[i].x < points[minIndex].x)
            {
                yMin = points[i].y;
                minIndex = i;
            }
        }

        Utilities.Swap(0, minIndex, points);

        P0 = points[0];

        PointComparer pointComparer = new PointComparer();
        points.Sort(1, numberOfPoints - 1, pointComparer);

        int modifiedArraySize = 1;
        for (int i = 1; i < numberOfPoints; i++)
        {
            //Only Updates Array If The Point Has a Different Orientation
            while (i < numberOfPoints - 1 && Utilities.GetOrientation(P0, points[i], points[i + 1]) == Orientation.COLLINEAR)
            {
                i++;
            }

            points[modifiedArraySize] = points[i];
            modifiedArraySize++;
        }

        if (modifiedArraySize < 3) return;

        Stack<Vector2f> pointsInHull = new Stack<Vector2f>();
        pointsInHull.Push(points[0]);
        pointsInHull.Push(points[1]);
        pointsInHull.Push(points[2]);


        for (int i = 3; i < modifiedArraySize; i++)
        {
            // Keep removing top while the angle formed by
            // points next-to-top, top, and points[i] makes
            // a non-left turn
            while (pointsInHull.Count > 1 &&
                   Utilities.GetOrientation(Utilities.NextToTop(pointsInHull), pointsInHull.Peek(), points[i]) != Orientation.COUNTER_CLOCKWISE)
            {
                pointsInHull.Pop();
            }

            pointsInHull.Push(points[i]);
        }

        _pointsInHull = pointsInHull.ToList();
    }

    private void GenerateVoronoi(List<Vector2f> points)
    {
        Rectf bounds = new Rectf(-_pointsRange,-_pointsRange,_pointsRange * 2,_pointsRange * 2);

        Voronoi voronoi = new Voronoi(points,bounds);

        _voronoi = voronoi;
        _voronoiEdges = voronoi.Edges;
    }
}