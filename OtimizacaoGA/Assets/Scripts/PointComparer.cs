﻿using System.Collections.Generic;

public class PointComparer : IComparer<Vector2f>
{
    public int Compare(Vector2f point1, Vector2f point2)
    {
        int orientation = (int)Utilities.GetOrientation(GraphManager.P0, point1, point2);

        if (orientation == 0)
        {
            return Vector2f.Distance(GraphManager.P0, point2) >= Vector2f.Distance(GraphManager.P0, point1) ? -1 : 1;
        }

        return (orientation == 2) ? -1 : 1;
    }
}