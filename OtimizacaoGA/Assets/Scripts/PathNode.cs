using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PathNode
{
    public PathNode CameFromNode;
    [NonSerialized]public List<PathNode> Neighbours = new List<PathNode>();

    public float gCost;
    public float hCost;
    public float fCost => gCost + hCost;

    public Vector2f Coordinates;

    public void Reset()
    {
        CameFromNode = null;

        gCost = float.PositiveInfinity;
        hCost = 0;
    }
    public PathNode(Vector2f coordinates)
    {
        Coordinates = coordinates;

        Neighbours = new List<PathNode>();
        CameFromNode = null;

        gCost = float.PositiveInfinity;
        hCost = 0;
    }
}
