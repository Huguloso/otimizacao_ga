using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILineRenderer : Graphic
{
    public Vector2 GridSize;

    public List<Vector2> Points;

    private float _width;
    private float _height;
    private float _unitWidth;
    private float _unitHeight;

    public float Thickness = 10f;

    protected override void OnPopulateMesh(VertexHelper vh)
    {
        vh.Clear();

        _width = rectTransform.rect.width;
        _height = rectTransform.rect.height;

        _unitWidth = _width / GridSize.x;
        _unitHeight = _height / GridSize.y;

        if (Points.Count < 2)
        {
            return;
        }

        float angle = 0;
        for (int i = 0; i < Points.Count; i++)
        {
            Vector2 point = Points[i];

            if (i < Points.Count - 1)
            {
                angle = GetAngle(Points[i], Points[i + 1] )+ 45f;
            }
            DrawVerticesForPoint(point, vh, angle);
        }
        
        for (int i = 0; i < Points.Count - 1; i++)
        {
            int index = i * 2;
            vh.AddTriangle(index + 0, index + 1, index + 3);
            vh.AddTriangle(index + 3, index + 2, index + 0);
        }
    }

    private float GetAngle(Vector2 me, Vector2 target)
    {
        return Mathf.Atan2(target.y - me.y, target.x - me.x) * (180 / Mathf.PI);
    }

    private void DrawVerticesForPoint(Vector2 point, VertexHelper vertexHelper, float angle)
    {
        UIVertex vertex = UIVertex.simpleVert;
        vertex.color = color;

        vertex.position = Quaternion.Euler(0, 0, angle) * new Vector3(-Thickness / 2, 0);
        vertex.position += new Vector3(_unitWidth * point.x, _unitHeight * point.y);
        vertexHelper.AddVert(vertex);
        
        vertex.position = Quaternion.Euler(0, 0, angle) * new Vector3(Thickness / 2, 0);
        vertex.position += new Vector3(_unitWidth * point.x, _unitHeight * point.y);
        vertexHelper.AddVert(vertex);
    }

}
