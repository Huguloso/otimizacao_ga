using System;
using System.Collections;
using System.Collections.Generic;
using csDelaunay;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [Header("Scene References")]
    [SerializeField] private GraphManager _graphManager;
    [SerializeField] private RectTransform _pointsPanel;
    [SerializeField] private RectTransform _linesPanel;
    [SerializeField] private UILineRenderer _hullLineRenderer;

    [Header("Prefabs")]
    [SerializeField] private PointUIButton _pointPrefab;
    [SerializeField] private UILineRenderer _lineRendererPrefab;

    [Header("Colors")]
    [SerializeField] private Color _hullColor;
    [SerializeField] private Color _voronoiColor;
    [SerializeField] private Color _connectionColor;
    [SerializeField] private Color _pathColor = Color.magenta;

    [Header("Thickness")]
    [SerializeField] private float _hullThickness = 10;
    [SerializeField] private float _voronoiThickness = 10;
    [SerializeField] private float _connectionThickness = 10;
    [SerializeField] private float _pathThickness = 10;


    private readonly List<PointUIButton> _pointButtons = new List<PointUIButton>();
    private readonly List<UILineRenderer> _voronoiLines = new List<UILineRenderer>();
    private readonly List<UILineRenderer> _connectionLines = new List<UILineRenderer>();
    private readonly List<UILineRenderer> _pathLines = new List<UILineRenderer>();

    private bool _voronoiActive = true;
    private bool _connectionActive = true;

    private PathNode _startNode;
    private PathNode _endNode;

    public void OnClick_RandomizePoints()
    {
        _graphManager.InitializeGraph();
        DestroyPathLines();
        InstantiatePoints();
        InitializeVoronoi();
        InitializeConnections();
        InitializeHull();
    }

    public void OnClick_DisplayConvexHull()
    {
        _hullLineRenderer.gameObject.SetActive(!_hullLineRenderer.isActiveAndEnabled);
    }

    public void OnClick_Voronoi()
    {
        _voronoiActive = !_voronoiActive;

        foreach (UILineRenderer voronoiLine in _voronoiLines)
        {
            voronoiLine.gameObject.SetActive(_voronoiActive);
        }
    }

    public void OnClick_DisplayConnections()
    {
        _connectionActive = !_connectionActive;

        foreach (UILineRenderer connection in _connectionLines)
        {
            connection.gameObject.SetActive(_connectionActive);
        }
    }

    private void Start()
    {
        InstantiatePoints();
        InitializeVoronoi();
        InitializeConnections();
        InitializeHull();
    }

    private void InstantiatePoints()
    {
        if (_pointButtons.Count > 0)
        {
            foreach (PointUIButton pointUIButton in _pointButtons)
            {
                Destroy(pointUIButton.gameObject);
            }

            _pointButtons.Clear();
        }

        _startNode = null;
        _endNode = null;

        foreach (KeyValuePair<Vector2f, PathNode> entry in Pathfinding.PathNodesDictionary)
        {
            PointUIButton pointUIButton = Instantiate(_pointPrefab, _pointsPanel);

            pointUIButton.InitializePathNode(entry.Value);

            pointUIButton.OnPointClicked += HandlePointClicked;
            pointUIButton.RectTransform.anchoredPosition = new Vector2(entry.Key.x, entry.Key.y);
            _pointButtons.Add(pointUIButton);
        }
    }

    private void InitializeHull()
    {
        _hullLineRenderer.gameObject.SetActive(false);
        _hullLineRenderer.Points.Clear();
        _hullLineRenderer.color = _hullColor;
        _hullLineRenderer.GridSize = _linesPanel.rect.size;
        _hullLineRenderer.Thickness = _hullThickness;
        foreach (Vector2f point in _graphManager.PointsInHull)
        {
            _hullLineRenderer.Points.Add(new Vector2(point.x, point.y));
        }

        _hullLineRenderer.Points.Add(new Vector2(_graphManager.PointsInHull[0].x, _graphManager.PointsInHull[0].y));
        _hullLineRenderer.gameObject.SetActive(true);
        _hullLineRenderer.transform.SetSiblingIndex(int.MaxValue);
    }

    private void InitializeVoronoi()
    {
        if (_voronoiLines.Count > 0)
        {
            foreach (UILineRenderer voronoiLine in _voronoiLines)
            {
                Destroy(voronoiLine.gameObject);
            }

            _voronoiLines.Clear();
        }

        foreach (Edge edge in _graphManager.VoronoiEdges)
        {
            if (edge.ClippedEnds == null)
            {
                continue;
            }

            UILineRenderer voronoiLine = Instantiate(_lineRendererPrefab, _linesPanel);

            voronoiLine.color = _voronoiColor;
            voronoiLine.GridSize = _linesPanel.rect.size;
            voronoiLine.Thickness = _voronoiThickness;
            voronoiLine.Points.Add(new Vector2(edge.ClippedEnds[LR.LEFT].x, edge.ClippedEnds[LR.LEFT].y));
            voronoiLine.Points.Add(new Vector2(edge.ClippedEnds[LR.RIGHT].x, edge.ClippedEnds[LR.RIGHT].y));
            _voronoiLines.Add(voronoiLine);
        }
    }

    private void InitializeConnections()
    {
        if (_connectionLines.Count > 0)
        {
            foreach (UILineRenderer connectionLine in _connectionLines)
            {
                Destroy(connectionLine.gameObject);
            }

            _connectionLines.Clear();
        }

        foreach (KeyValuePair<Vector2f, PathNode> entry in Pathfinding.PathNodesDictionary)
        {
            PathNode currentNode = entry.Value;
            foreach (PathNode neighbour in currentNode.Neighbours)
            {
                UILineRenderer connectionLine = Instantiate(_lineRendererPrefab, _linesPanel);

                connectionLine.color = _connectionColor;
                connectionLine.GridSize = _linesPanel.rect.size;
                connectionLine.Thickness = _connectionThickness;
                connectionLine.Points.Add(new Vector2(neighbour.Coordinates.x, neighbour.Coordinates.y));
                connectionLine.Points.Add(new Vector2(currentNode.Coordinates.x, currentNode.Coordinates.y));
                _connectionLines.Add(connectionLine);
            }
        }
    }

    private void HandlePointClicked(PathNode pathNode)
    {
        if (_startNode == null)
        {
            _startNode = pathNode;
            _endNode = null;
        }
        else if (_endNode == null)
        {
            _endNode = pathNode;
            List<PathNode> path = Pathfinding.FindPath(_startNode.Coordinates, _endNode.Coordinates);
            DrawPath(path);

            _startNode = null;
        }
    }

    private void DrawPath(List<PathNode> path)
    {
        DestroyPathLines();

        for (int i = 0; i < path.Count; i++)
        {
            if (i + 1 == path.Count)
            {
                return;
            }

            UILineRenderer pathLine = Instantiate(_lineRendererPrefab, _linesPanel);

            pathLine.color = _pathColor;
            pathLine.GridSize = _linesPanel.rect.size;
            pathLine.Thickness = _pathThickness;
            pathLine.Points.Add(new Vector2(path[i].Coordinates.x, path[i].Coordinates.y));
            pathLine.Points.Add(new Vector2(path[i + 1].Coordinates.x, path[i + 1].Coordinates.y));
            _pathLines.Add(pathLine);
        }
    }

    private void DestroyPathLines()
    {
        if (_pathLines.Count > 0)
        {
            foreach (UILineRenderer pathLine in _pathLines)
            {
                Destroy(pathLine.gameObject);
            }

            _pathLines.Clear();
        }
    }
}
