﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PointUIButton : MonoBehaviour
{
    public event Action<PathNode> OnPointClicked;

    [SerializeField] private PathNode _pathNode;
    private Button _button;

    public RectTransform RectTransform { get; private set; }

    private void Awake()
    {
        _button = GetComponent<Button>();
        RectTransform = GetComponent<RectTransform>();
        _button.onClick.AddListener(() => OnPointClicked?.Invoke(_pathNode));
    }

    public void InitializePathNode(PathNode pathNode)
    {
        _pathNode = pathNode;
    }
}
