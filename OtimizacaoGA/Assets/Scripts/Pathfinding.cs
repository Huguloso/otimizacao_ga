using System.Collections.Generic;
using csDelaunay;
using UnityEngine;

public static class Pathfinding
{
    private static Dictionary<Vector2f, PathNode> _pathNodesDictionary = new Dictionary<Vector2f, PathNode>();

    private static List<PathNode> _openList;
    private static List<PathNode> _closedList;

    public static Dictionary<Vector2f, PathNode> PathNodesDictionary => _pathNodesDictionary;

    public static void GeneratePathNodes(Dictionary<Vector2f, Site> sites)
    {
        _pathNodesDictionary = new Dictionary<Vector2f, PathNode>();

        foreach (KeyValuePair<Vector2f, Site> entry in sites)
        {
            PathNode pathNode = new PathNode(entry.Value.Coord);
            _pathNodesDictionary.Add(pathNode.Coordinates, pathNode);
        }

        foreach (KeyValuePair<Vector2f, Site> entry in sites)
        {
            List<PathNode> neighbourNodes = new List<PathNode>();
            foreach (Edge edge in entry.Value.Edges)
            {
                if (edge.LeftSite.Coord == entry.Value.Coord)
                {
                    neighbourNodes.Add(_pathNodesDictionary[edge.RightSite.Coord]);
                }
                else
                {
                    neighbourNodes.Add(_pathNodesDictionary[edge.LeftSite.Coord]);
                }
            }

            _pathNodesDictionary[entry.Value.Coord].Neighbours = neighbourNodes;
        }
    }

    public static List<PathNode> FindPath(Vector2f startCoordinates, Vector2f endCoordinates)
    {
        if (_pathNodesDictionary.Count == 0)
        {
            Debug.LogError("Path Node Dictionary is empty");
        }

        PathNode startNode = _pathNodesDictionary[startCoordinates];
        PathNode endNode = _pathNodesDictionary[endCoordinates];

        _openList = new List<PathNode>{startNode};
        _closedList = new List<PathNode>();

        foreach (var entry in _pathNodesDictionary)
        {
            entry.Value.Reset();
        }

        startNode.gCost = 0;
        startNode.hCost = CalculateHCost(startNode, endNode);

        while (_openList.Count > 0)
        {
            PathNode currentNode = GetLowestFCostNode(_openList);

            if (currentNode == endNode)
            {
                return CalculatePath(endNode);
            }

            _openList.Remove(currentNode);
            _closedList.Add(currentNode);

            foreach (PathNode neighbour in currentNode.Neighbours)
            {
                if (_closedList.Contains(neighbour))
                {
                    continue;
                }

                float tentativeGCost = currentNode.gCost + CalculateHCost(currentNode, neighbour);

                if (tentativeGCost < neighbour.gCost)
                {
                    neighbour.CameFromNode = currentNode;
                    neighbour.gCost = tentativeGCost;
                    neighbour.hCost = CalculateHCost(neighbour, endNode);

                    if (!_openList.Contains(neighbour))
                    {
                        _openList.Add(neighbour);
                    }
                }
            }
        }

        Debug.Log("Could Not Find Path");
        return new List<PathNode>();
    }

    private static List<PathNode> CalculatePath(PathNode endNode)
    {
        List<PathNode> path = new List<PathNode> {endNode};
        PathNode currentNode = endNode;

        while (currentNode.CameFromNode != null)
        {
            path.Add(currentNode.CameFromNode);
            currentNode = currentNode.CameFromNode;
        }

        path.Reverse();
        return path;
    }

    private static float CalculateHCost(PathNode node, PathNode endNode)
    {
        return Vector2f.Distance(node.Coordinates, endNode.Coordinates);
    }

    private static PathNode GetLowestFCostNode(List<PathNode> pathNodes)
    {
        PathNode lowestFCostNode = pathNodes[0];

        foreach (PathNode pathNode in pathNodes)
        {
            if (pathNode.fCost < lowestFCostNode.fCost)
            {
                lowestFCostNode = pathNode;
            }
        }

        return lowestFCostNode;
    }
}
