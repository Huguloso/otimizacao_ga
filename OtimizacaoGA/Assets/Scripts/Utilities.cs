﻿using System.Collections.Generic;

public static class Utilities
{
    public static void Swap(int index1, int index2, List<Vector2f> pointsList)
    {
        Vector2f temp = pointsList[index1];
        pointsList[index1] = pointsList[index2];
        pointsList[index2] = temp;
    }

    public static Orientation GetOrientation(Vector2f p, Vector2f q, Vector2f r)
    {
        float val = (q.y - p.y) * (r.x - q.x) -
                    (q.x - p.x) * (r.y - q.y);

        if (val == 0)
        {
            return Orientation.COLLINEAR;
        }

        return val > 0 ? Orientation.CLOCKWISE : Orientation.COUNTER_CLOCKWISE;
    }

    public static Vector2f NextToTop(Stack<Vector2f> s)
    {
        Vector2f p = s.Peek();
        s.Pop();
        Vector2f res = s.Peek();
        s.Push(p);
        return res;
    }
}

public enum Orientation
{
    COLLINEAR = 0,
    CLOCKWISE = 1,
    COUNTER_CLOCKWISE = 2
}